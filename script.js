/*
Задание

Добавить в домашнее задание HTML/CSS №6 (Flex/Grid) различные эффекты с использованием jQuery

Технические требования:

1. Добавить вверху страницы горизонтальное меню со ссылкой на все разделы страницы. При клике на ссылку - плавно прокручивать страницу вниз до выбранного места.
2. Если прокрутить страницу больше чем на 1 экран вниз, справа внизу должна появляться кнопка "Наверх" с фиксированным позиционариванием. При клике на нее - плавно прокрутить страницу в самый верх.
3. Добавить под одной из секций кнопку, которая будет выполнять функцию slideToggle() (прятать и показывать по клику) для данной секции.
*/


//Creating anchor links
$('a[href^="#"]').click(function () {
    $('html, body').animate({
        scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top
    }, 800);

    return false;
});


//Crating scroll to top button
$(document).ready(function(){

    //Check to see if the window is top if not then display button
    $(window).scroll(function(){
        if ($(this).scrollTop() > window.innerHeight) {
            $('.scroll-to-top').fadeIn();
        } else {
            $('.scroll-to-top').fadeOut();
        }
    });

    //Click event to scroll to top
    $(".scroll-to-top").click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });
});

//Creating slide toggle button
$(document).ready(function(){
    $(".slide-toggle-btn").click(function(){
        $(".hot-news").slideToggle();
    });
});